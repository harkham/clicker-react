
import dashboard from './dashboard';
import { combineReducers } from 'redux';

const reducers = combineReducers({
    dashboard
});

export default reducers