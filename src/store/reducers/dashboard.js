import { ADD_LINE, REMOVE_LINE } from '../constants/ActionTypes'

const initialState = {
    lineNumber: 0,
};

export default function dashboard(state = initialState, action) { 
    switch(action.type){
        case ADD_LINE:
            return {
                lineNumber : state.lineNumber + action.value
            };
        case REMOVE_LINE:
            return {
                lineNumber: state.lineNumber - action.value
            };
        default:
            return state;
    }
}