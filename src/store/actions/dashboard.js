import * as types from '../constants/ActionTypes';

export const addLine = (value) => ({ type: types.ADD_LINE, value });
export const removeLine = (value) => ({ type: types.REMOVE_LINE, value });