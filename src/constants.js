
export const APP_NAME = 'clicker'
export const LINE_NUMBER_UNLOCK_EMPLOYEE = 100
export const PLAYER_PROJECT_GOAL = 50000
export const updateFramerate = 60
export const fixedUpdateFramerate = 30
export const initValueWorkEmployer = 4 // ms  ( 1 line by x )
export const EMPLOYEE_PRICE = 2000 // $ /mouth
export const COMMERCIAL_PRICE = 2000 // $ /mouth
export const secondByDay = 5000 // ms
export const START_MONEY = 2000 // $
export const START_CHARGE = 0 // $
export const baseDurationTrainingEmployee = 500 // ms
export const BASE_PRICE_TRAINING_EMPLOYEE = 50000 // $
export const BASE_TIME_TRAINING_DEVELOPER = 40 // s
export const BASE_TIME_TRAINING_COMMERCIAL = 40 // $
export const BASE_TIME_UPGRADE_LOCAL = 40 // $
export const INCREASE_EFFICIENCY_NEW_EMPLOYEE_LEVEL = 1.5
export const INCREASE_EFFICIENCY_NEW_COMMERCIAL_LEVEL = 1.5
export const MULTIPLICATOR_PRICE_NEW_EMPLOYEE_LEVEL = 2
export const PROJECT_1_PRICE_PROJECT = 2000
export const PROJECT_1_LINE_REQUEST_PROJECT = 500
export const PROJECT_2_PRICE_PROJECT = 5000
export const PROJECT_2_LINE_REQUEST_PROJECT = 1000
export const PROJECT_3_PRICE_PROJECT = 24000
export const PROJECT_3_LINE_REQUEST_PROJECT = 4000
export const priceLocal2 = 20000 // $
export const unlockBuyEmployee = 1 // line
export const unlockTrainingEmployee = 5 // employee
export const unlockLocal2 = 20000 // $
export const unlockEngageCommercial = 1 // mouth
export const unlockBuyLocal2 = 8 // employee
export const WORK_SPEED_EMPLOYEE = 3 // (x / seconde )
export const WORK_SPEED_COMMERCIAL = 2 // ( x / seconde )
export const UPDATE_FRAME_PER_SECOND = 17
export const FIXEDUPDATE_FRAME_PER_SECOND = 50
export const EMPLOYEE_TRAINING_1 = 20000

export const ALERT_DURATION = 10000
export const SECOND_BY_DAY = 4
export const BASE_TIME_TRAINING_DEVELOPER_IN_DAY = (BASE_TIME_TRAINING_DEVELOPER / SECOND_BY_DAY).toFixed(0)
export const BASE_TIME_UPGRADE_LOCAL_IN_DAY = (BASE_TIME_UPGRADE_LOCAL / SECOND_BY_DAY).toFixed(0)

export const PROJECTS = {
  SERVICE_PROJECT: 1,
  PLAYER_PROJECT: 2
}
