import React from 'react';
import { connect } from 'react-redux';
import store from '../store/store';
import { addLine } from './../store/actions/dashboard';
 
class DashBoard extends React.Component {

    render(){
        return(
            <div>
                <p>LOL</p>
                <p>{this.props.lineNumber}</p>
                <button onClick={() => this.addLine(1)}>MDR</button>
            </div>
        )
    }

    addLine(value)
    {
        console.log('mdr')
        this.props.addLine(value)
    }
}

const mapDispatchToProps = dispatch => ({
    addLine: number => dispatch(addLine(number))
})

const mapStateToProps = state => ({
    lineNumber: state.dashboard.lineNumber
})

export default connect(mapStateToProps, mapDispatchToProps)(DashBoard)
