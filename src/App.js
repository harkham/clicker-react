import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { addLine } from './store/actions/dashboard';
import store from './store/store';
import DashBoard from './components/DashBoard';
import {Provider} from 'react-redux'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
            <header className="App-header">
              <DashBoard/>
            <img src={logo} className="App-logo" alt="logo" />
          </header>
        </div>
      </Provider>
    );
  }
}
export default App;
